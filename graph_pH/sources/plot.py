from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response

from django.shortcuts import render
from chartit import DataPool, Chart



# plot a chart with django-chartit

class Plot:

    def __init__(self, model):
        self.model= model
    

    def chart_view(self):
        #Step 1: Create a DataPool with the data we want to retrieve.
        legend = self.model
        legend.legend = "ph1"
        dataplot = \
        DataPool(
                 series=
                 [{'options': {
                  'source': self.model.objects.all()},
                  'terms': [
                            'ph',
                            'rh2',
                            'orp']}
                  ])

    #Step 2: Create the Chart object
        cht = Chart(
                datasource = dataplot,
                series_options =
                [{'options':{
                 'type': 'scatter',
                 'stacking': False},
                 'terms':{
                 'ph': ['rh2',
                        'orp']
                 }}],
                chart_options =
                {
                'title':{'text': 'Graphic rH2=f(pH)'},
                'yAxis': {'title':{'text':'rH2'}},
                'xAxis': {'title': {'text': 'pH'}}
                }
                    )

#Step 3: Send the chart object to the template.
        return cht



