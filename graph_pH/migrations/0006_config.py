# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2017-05-13 13:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('graph_pH', '0005_auto_20170512_0539'),
    ]

    operations = [
        migrations.CreateModel(
            name='config',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('path_file_csv', models.CharField(max_length=200)),
            ],
        ),
    ]
