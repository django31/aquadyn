from __future__ import unicode_literals

from django.apps import AppConfig


class GraphPhConfig(AppConfig):
    name = 'graph_pH'
