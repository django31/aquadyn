from django.conf.urls import include, url
from . import views
from . import models



urlpatterns = [
    url(r'^$',views.home),
    url(r'^setval$',views.DataView.as_view()),
    url(r'^success$',views.success_form),
    url(r'^config$',views.ConfigView.as_view()),
    url(r'^manualplot$',views.manualplot),
    url(r'^wattmonplot$',views.plot_wattmonplot),
    url(r'^resetwattmon$',views.reset_wattmon),
    url(r'^resetmanual$',views.reset_manual),
    url(r'^test$',views.test)
               

]
