from django.shortcuts import render, redirect
from django.conf.urls import url


import os
from django.shortcuts import render_to_response
from django.http import HttpResponse, HttpRequest, JsonResponse
from django.views.generic import *
#models
from models import data_manual,data_wattmon, config #, data_manual_form, ConfigForm
#forms
from forms import data_manual_form, ConfigForm, TestForm


#reseau
from uuid import getnode as get_mac
import netifaces as nif
#chartit graph
from chartit import DataPool, Chart
#time and date
from datetime import datetime
#csv
import csv
#sources
from graph_pH.sources.plot import Plot
from graph_pH.sources.timer import Timer
from graph_pH.sources.sql import Database_sqlite as db

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# back view after fill formss
def success_form(request):
    query_model = data_manual.objects
    pH = query_model.values_list('ph')
    rh2 = query_model.values_list('rh2')
    for i in query_model.all():
        print("success_form: %s"%i)
    
    return render(request,'graph_pH/success_form.html')

# home page
def home(request):
    return render(request,'graph_pH/home.html',{'date' :datetime.now()})

def reset_manual(request):
    Db=db(name=os.path.join(BASE_DIR,'aquadyn_webapp.db'))
    Db.delete_tab('graph_pH_data_manual')
    return render(request,'graph_pH/success_reset_manual.html')

def reset_wattmon(request):
    ### protection
    Db=db(name=os.path.join(BASE_DIR,'aquadyn_webapp.db'))
    Db.delete_tab('graph_pH_data_manual')
    return render(request,'graph_pH/success_reset_wattmon.html')

## start timer and read csv


### GET IP and MAC on the network
### NO USE
def mac_for_ip(ip):
    'Returns a list of MACs for interfaces that have given IP, returns None if not found'
    print(nif.interfaces)
    for i in nif.interfaces():
        addrs = nif.ifaddresses(i)
        print(addrs)
    return None

### PLOT a graph with mual setting data coming = models.data_manual
def manualplot(request):
    class_plot = Plot(model=data_manual)
    cht=class_plot.chart_view()
    return render_to_response('graph_pH/plotdata_manual.html',{'chart': cht})

### PLOT the graph from the wattmon data coming = models.data_wattmon FILE CSV data.csv
class Wattmon:
    
    def __init__(self,path=os.path.join(BASE_DIR,'data.csv')):
        self.Db=db(name=os.path.join(BASE_DIR,'aquadyn_webapp.db'))
        self.csv_path=path
    ## rajouter l'init du fichier de lecture


    def wattmonplot(self):
        self.load_data_from_csv()
        class_plot = Plot(model=data_wattmon)
        cht=class_plot.chart_view()
        return cht
    
### donne le nombre de ligne
    def getlinenb(self):
        with open(self.csv_path,'rb') as datafile:
            reader=csv.reader(datafile)
            for line in reader:
                line_max=reader.line_num
        return line_max

### LOAD DATA on the model data and initialise it
    def load_data_from_csv(self):
        self.Db.delete_tab('graph_pH_data_wattmon')
        with open(self.csv_path,'rb') as datafile:
            reader=csv.reader(datafile)
            for line in reader:
                #### protection si ph>14 ou <0 opr > 20
                rh2_calc=2*float(line[1]) + (1/3)*float(line[2])
                data_model = data_wattmon(ph=float(line[1]),rh2=rh2_calc,orp=float(line[2 ]))
                data_model.save()
        view = data_wattmon.objects.all()
        for a in view:
            print(a.ph, a.rh2, a.orp)
    #nombre de donnee lu
        nb_data=self.getlinenb()
        print("nombre de ligne lues %s" %nb_data)
    #on change la derniere lecture
        return nb_data

####
def plot_wattmonplot(request):
    class_config= ConfigForm()
    path = class_config.getpath()
    plot=Wattmon(path)
    cht = plot.wattmonplot()
    return render_to_response('graph_pH/plotdata_wattmon.html',{'chart': cht})

def get(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect('/thanks/')

# if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()
    
    return render(request, 'view.html', {'form': form})

def test(request):
    return render(request,'graph_pH/test_getwattmon.html')



### config form
class ConfigView(FormView):
    template_name = 'graph_pH/config.html'
    form_class = ConfigForm
    success_url = 'wattmonplot'
    
    def form_valid(self, form):
        Db=db(name=os.path.join(BASE_DIR,'aquadyn_webapp.db'))
        Db.delete_tab('graph_pH_config')
        form.display()
        if(form.getpath()!=0):
            print("csv path valid")
        return super(ConfigView, self).form_valid(form)


### data view for set data
class DataView(FormView):
    template_name = 'graph_pH/setdata.html'
    form_class = data_manual_form
    success_url = 'manualplot'

    def form_valid(self, form):
        form.display()
        return super(DataView, self).form_valid(form)

#### test NO USE
class TestView(FormView):
    template_name = 'graph_pH/test_getwattmon.html'
    form_class = TestForm
    success_url = 'graph_pH/success_form.html'









