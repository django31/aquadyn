from __future__ import unicode_literals

from django.db import models
from django import forms
from django.forms import ModelForm
import django.core.management as manage
#sources
from graph_pH.sources.sql import Database_sqlite as db
# form

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Save data from manual set value
class data_manual(models.Model):
    ph = models.DecimalField(max_digits=30, decimal_places=1)
    rh2 = models.DecimalField(max_digits=30, decimal_places=1)
    orp = models.DecimalField(max_digits=30, decimal_places=1)

# Save data from csv file wrote with wattmon data
class data_wattmon(models.Model):
    ph = models.DecimalField(max_digits=5, decimal_places=1)
    rh2 = models.DecimalField(max_digits=5, decimal_places=1)
    orp = models.DecimalField(max_digits=5, decimal_places=1)

    def save(self, *args, **kwargs):
        #do_something()
        super(data_wattmon, self).save(*args, **kwargs) # Call the "real" save() method.

# Save csv path
class config(models.Model):
    path_file_csv = models.CharField(max_length=200)

    def __str__(self):
        return self.path_file_csv

class csv(models.Model):
    file_upload = models.FileField(upload_to='uploads/')

