
from django.db import models
from django import forms
from django.forms import ModelForm
import django.core.management as manage
from django.views.generic import *
#sources
from graph_pH.sources.sql import Database_sqlite as db
# models
from models import data_manual, data_wattmon, config, csv

import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))



# form to get manual set values
class data_manual_form(ModelForm):
    class Meta:
        model = data_manual
        fields = ['ph', 'orp']
    
    def display(self):
        f=self.save(commit=False)
        f.rh2 = 2*self.cleaned_data['ph'] + self.cleaned_data['orp']/3
        f.save()
        self.save_m2m()
        self.cal_rh2()

    def cal_rh2(self):
        view=data_manual.objects.all()
        for i in view:
            print(i.ph,i.orp,i.rh2)

# form to config csv path
class ConfigForm(ModelForm):
    class Meta:
        model = config
        fields = ['path_file_csv']
    
    def display(self):
        print self.cleaned_data
        self.save()
    
    def getpath(self):
        z=config.objects.all()
        path="%s"%z[0]
        if(open(path)):
            return path
        else:
            return 0


class TestForm(ModelForm):
    class Meta:
        model = csv
        fields = ['file_upload']

    def display(self):
        print self.cleaned_data





